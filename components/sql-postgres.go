package components

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

// MustPostgres panics or returns a *sql.DB
func MustPostgres(host, user, password, dbName, sslMode string, port int, customDriver ...string) *sql.DB {
	db, err := Postgres(host, user, password, dbName, sslMode, port, customDriver...)
	if err != nil {
		panic(err)
	}
	return db
}

// Postgres  returns a *sql.DB , or error
func Postgres(host, user, password, dbName, sslMode string, port int, customDriver ...string) (*sql.DB, error) {
	driver := sqlDriverPostgres
	if len(customDriver) != 0 {
		driver = customDriver[0]
	}
	dsn := sqlDsn(
		host, user, password, dbName,
		sslMode, port,
	)

	return sql.Open(driver, dsn)
}

// PostgresX returns a *sqlx.DB pointer, or error
func PostgresX(host, user, password, dbName, sslMode string, port int, customDriver ...string) (*sqlx.DB, error) {
	db, err := Postgres(host, user, password, dbName, sslMode, port, customDriver...)
	if err != nil {
		return nil, err
	}
	return sqlx.NewDb(db, sqlDriverPostgres), nil
}
func MustPostgresX(host, user, password, dbName, sslMode string, port int, customDriver ...string) *sqlx.DB {
	dbx, err := PostgresX(host, user, password, dbName, sslMode, port, customDriver...)
	if err != nil {
		panic(err)
	}
	return dbx
}
