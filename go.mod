module gitlab.com/go-helpers/utility

go 1.16

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.2
	github.com/stretchr/testify v1.7.0
	gopkg.in/eapache/go-resiliency.v1 v1.2.0
)
