package httpx_test

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-helpers/utility/httpx"
)

var testServer = httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, client")
}))

func TestGet(t *testing.T) {
	tt := &topt{
		asserter: assert.New(t),
	}
	err := httpx.Get(tt)
	tt.asserter.NoError(err)
}
func TestPut(t *testing.T) {
	tt := &topt{
		asserter: assert.New(t),
	}
	err := httpx.Put(tt)
	tt.asserter.NoError(err)
}
func TestPost(t *testing.T) {
	tt := &topt{
		asserter: assert.New(t),
	}
	err := httpx.PostWithRetry(tt, 2, time.Millisecond*2)
	tt.asserter.NoError(err)
}

type topt struct {
	asserter *assert.Assertions
}

func (*topt) Endpoint() string {
	return testServer.URL

}
func (*topt) Timeout() *time.Duration {
	return nil
}
func (*topt) Headers() map[string]string {
	return nil
}
func (*topt) QueryParams() map[string]interface{} {
	return nil
}
func (*topt) Body() io.Reader {
	return nil
}

// Actions
func (*topt) ParseHeaders(http.Header) error {
	return nil
}
func (t *topt) ParseBody(i io.ReadCloser) error {
	bod, err := ioutil.ReadAll(i)
	if err != nil {
		return err
	}
	t.asserter.Equal(string(bod), "Hello, client")
	return nil
}
func (*topt) ParseStatusCode(i int) {
}
func (*topt) Log(x int, y string, z interface{}) {
	fmt.Println("log", y, z)
}
