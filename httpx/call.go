package httpx

import (
	"net/http"
	"time"

	"gopkg.in/eapache/go-resiliency.v1/retrier"
)

func call(method string, t Transaction) (err error) {
	var (
		req        *http.Request
		res        *http.Response
		startTime  = time.Now()
		statusCode int
		header     http.Header
	)

	defer func() {
		t.Log(1, "http request result", map[string]interface{}{
			"method":     method,
			"endpoint":   t.Endpoint(),
			"res.code":   statusCode,
			"res.header": header,
			"time":       time.Since(startTime),
		})
	}()

	if req, err = newRequest(method, t); err != nil {
		return err
	}

	if res, err = do(newClient(t.Timeout()), req); err != nil {
		return err
	}
	statusCode = res.StatusCode
	header = res.Header
	defer res.Body.Close()
	return parse(res, t)
}
func callRetry(method string, t Transaction, retries int, backoff time.Duration) (err error) {
	var (
		req        *http.Request
		res        *http.Response
		r          = retrier.New(retrier.ConstantBackoff(retries, backoff), nil)
		iteration  int
		startTime  = time.Now()
		statusCode int
		header     http.Header
	)
	defer func() {
		t.Log(1, "http(with retry) request result", map[string]interface{}{
			"method":     method,
			"endpoint":   t.Endpoint(),
			"iteration":  iteration,
			"res.code":   statusCode,
			"res.header": header,
			"time":       time.Since(startTime),
		})
	}()

	if req, err = newRequest(method, t); err != nil {
		return err
	}

	err = r.Run(func() error {
		// do some work
		res, err = do(newClient(t.Timeout()), req)
		iteration++
		return err
	})

	if err != nil {
		return err
	}
	statusCode = res.StatusCode
	header = res.Header
	defer res.Body.Close()
	return parse(res, t)
}

func parse(res *http.Response, t Transaction) error {
	t.ParseHeaders(res.Header)
	t.ParseStatusCode(res.StatusCode)
	return t.ParseBody(res.Body)
}

func do(client *http.Client, req *http.Request) (*http.Response, error) {
	return client.Do(req)
}

func newRequest(method string, t Transaction) (req *http.Request, err error) {
	req, err = http.NewRequest(method, t.Endpoint(), t.Body())
	if err != nil {
		return nil, err
	}
	setQueryParams(req, t.QueryParams())
	setHeaders(req, t.Headers())

	return req, nil
}
