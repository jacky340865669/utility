package httpx

import (
	"net/http"
	"time"
)

// Get deoes a http.Get request on transaction
func Get(t Transaction) (err error) {
	return call(http.MethodGet, t)
}

// GetWithRetry deoes a http.Get request on transaction, with exp backoff on retries
func GetWithRetry(t Transaction, retries int, backoff time.Duration) (err error) {
	return callRetry(http.MethodGet, t, retries, backoff)
}
