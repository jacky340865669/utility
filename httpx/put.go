package httpx

import (
	"net/http"
	"time"
)

// Put deoes a http.Post request on transaction
func Put(t Transaction) (err error) {
	return call(http.MethodPut, t)
}

// PutWithRetry deoes a http.Post request on transaction, with exp backoff on retries
func PutWithRetry(t Transaction, retries int, backoff time.Duration) (err error) {
	return callRetry(http.MethodPut, t, retries, backoff)
}
